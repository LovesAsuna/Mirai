package me.lovesasuna.bot.function.colorphoto;

public interface Source {
    /**
     * 从api获得数据
     * @return 从api获得数据
     **/
    String fetchData();
}
